<?php

declare(strict_types=1);

namespace DoctorI\Shared\Domain\ValueObject;

use DateTime;
use Stringable;

class Time implements ValueObject, Stringable
{
    public const DEFAULT_FORMAT = 'Y-m-d H:i:s';

    public function __construct(protected DateTime $value)
    {
    }

    public function value(): DateTime
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return $this->value->format(self::DEFAULT_FORMAT);
    }
}
