<?php

declare(strict_types=1);

namespace DoctorI\Shared\Domain\ValueObject;

use Stringable;

class Text implements ValueObject, Stringable
{
    public function __construct(protected string $value, protected ?bool $nullable = false)
    {
        $this->setValue($value);
    }

    public function value(): string
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return $this->value;
    }

    public function isNullable(): ?bool
    {
        return $this->nullable;
    }

    private function setValue(string $value): void
    {
        if (!$this->nullable) {
            $this->validate($value);
        }
        $this->value = trim($value);
    }

    private function validate(string $value): void
    {
        if (trim($value) === '') {
            throw new Exception\InvalidText($value);
        }
    }
}
