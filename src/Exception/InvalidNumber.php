<?php

declare(strict_types=1);

namespace DoctorI\Shared\Domain\ValueObject\Exception;

final class InvalidNumber extends DomainError
{
    private int $value;

    public function __construct(int $value)
    {
        $this->value = $value;
        parent::__construct();
    }

    public function errorCode(): string
    {
        return 'invalid_number';
    }

    protected function errorMessage(): string
    {
        return sprintf('Number field value <%s> is not a valid positive value.', $this->value);
    }
}
