<?php

declare(strict_types=1);

namespace DoctorI\Shared\Domain\ValueObject\Exception;

final class InvalidEmail extends DomainError
{
    private string $value;

    public function __construct(string $value)
    {
        $this->value = $value;
        parent::__construct();
    }

    public function errorCode(): string
    {
        return 'invalid_email';
    }

    protected function errorMessage(): string
    {
        return sprintf('The email <%s> is not a valid email address.', $this->value);
    }
}
