<?php

declare(strict_types=1);

namespace DoctorI\Shared\Domain\ValueObject\Exception;

use DomainException;

abstract class DomainError extends DomainException implements Exception
{
    public function __construct()
    {
        parent::__construct($this->errorMessage());
    }

    abstract public function errorCode(): string;

    abstract protected function errorMessage(): string;
}
