<?php

declare(strict_types=1);

namespace DoctorI\Shared\Domain\ValueObject\Exception;

final class InvalidMoney extends DomainError
{
    private float $value;

    public function __construct(float $value)
    {
        $this->value = $value;
        parent::__construct();
    }

    public function errorCode(): string
    {
        return 'invalid_money';
    }

    protected function errorMessage(): string
    {
        return sprintf('Money field value <%s> is not a valid positive value.', $this->value);
    }
}
