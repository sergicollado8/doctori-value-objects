<?php

declare(strict_types=1);

namespace DoctorI\Shared\Domain\ValueObject\Exception;

final class InvalidUuid extends DomainError
{
    private string $value;

    public function __construct(string $value)
    {
        $this->value = $value;
        parent::__construct();
    }

    public function errorCode(): string
    {
        return 'invalid_uuid';
    }

    protected function errorMessage(): string
    {
        return sprintf('Given string <%s> is not a valid Uuid.', $this->value);
    }
}
