<?php

declare(strict_types=1);

namespace DoctorI\Shared\Domain\ValueObject\Exception;

final class InvalidText extends DomainError
{
    private string $value;

    public function __construct(string $value)
    {
        $this->value = $value;
        parent::__construct();
    }

    public function errorCode(): string
    {
        return 'invalid_text';
    }

    protected function errorMessage(): string
    {
        return sprintf('Text field value <%s> is not a valid string.', $this->value);
    }
}
