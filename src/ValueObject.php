<?php

declare(strict_types=1);

namespace DoctorI\Shared\Domain\ValueObject;

interface ValueObject
{
    public function value(): mixed;
}
