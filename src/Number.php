<?php

declare(strict_types=1);

namespace DoctorI\Shared\Domain\ValueObject;

use DoctorI\Shared\Domain\ValueObject\Exception\InvalidNumber;
use Stringable;

class Number implements ValueObject, Stringable
{
    public function __construct(protected int $value)
    {
        $this->setValue($value);
    }

    public function value(): int
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return $this->value . '';
    }

    private function setValue(int $value): void
    {
        $this->validateNegativeValue($value);
        $this->value = $value;
    }

    private function validateNegativeValue(int $value): void
    {
        if ($value < 0) {
            throw new InvalidNumber($value);
        }
    }
}
