<?php

declare(strict_types=1);

namespace DoctorI\Shared\Domain\ValueObject;

use DoctorI\Shared\Domain\ValueObject\Exception\InvalidUuid;
use Ramsey\Uuid\Uuid as RamseyUuid;
use Stringable;

class Uuid implements ValueObject, Stringable
{
    public function __construct(protected string $value)
    {
        $this->validate($value);
    }

    public static function random(): self
    {
        return new self(RamseyUuid::uuid4()->toString());
    }

    public function value(): string
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return $this->value;
    }

    public function equals(Uuid $other): bool
    {
        return $this->value === $other->value();
    }

    private function validate(string $id): void
    {
        if (!RamseyUuid::isValid($id)) {
            throw new InvalidUuid($id);
        }
    }
}
