<?php

declare(strict_types=1);

namespace DoctorI\Shared\Domain\ValueObject;

use DoctorI\Shared\Domain\ValueObject\Exception\InvalidMoney;
use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Formatter\DecimalMoneyFormatter;
use Stringable;

class Money implements ValueObject, Stringable
{
    protected \Money\Money $value;

    public function __construct(protected float $valueAmount, protected string $currency = 'EUR')
    {
        $this->setValue($valueAmount);
    }

    public function value(): float
    {
        $moneyFormatter = new DecimalMoneyFormatter(new ISOCurrencies());
        return (float)$moneyFormatter->format($this->value);
    }

    public function currency(): string
    {
        return $this->currency;
    }

    public function __toString(): string
    {
        return $this->value() . ' ' . $this->currency();
    }

    private function setValue(float $value): void
    {
        $this->validateNegativeValue($value);
        $value = round($value, 2) * 100;
        $this->value = new \Money\Money((string)$value, new Currency($this->currency));
    }

    private function validateNegativeValue(float $value): void
    {
        if ($value < 0) {
            throw new InvalidMoney($value);
        }
    }
}
