# Value objects models

![source](https://img.shields.io/badge/source-doctori/value--objects-blue)
![version](https://img.shields.io/badge/version-1.0.0-blue.svg)
![php](https://img.shields.io/badge/php->=8.0-8892bf)

Value objects models to use in DoctorI applications.

## 🚀 Environment Setup

### 🛠️ Install dependencies
Install the dependencies if you haven't done it previously: `make build` or `composer install`

### ✅ Tests execution
Execute PHPUnit tests:
```bash
$ make run-test
```

### 🎨 Style
This project follows PSR-12 style
```bash
$ make check-style

$ make fix-style
```

### 🧐 Inspection
This project should be inspected by PHPStan
```bash
$ make inspect-phpstan
```

## 🔥 Usage

### 🛠️ Install in a third application
Include the package with composer.
```bash
$ composer require DoctorI/value-objects
```

#### Email

```php
<?php
$email = new DoctorI\Shared\Domain\ValueObject\Email('test@email.com');

// Output: test@email.com
$email->value();
```

#### Text

```php
<?php
$text = new DoctorI\Shared\Domain\ValueObject\Text('Text field');

// Output: Text field
$text->value();
```

#### Time

```php
<?php
$currentTime = new \DateTime('2019-10-09 18:31:00');

$time = new DoctorI\Shared\Domain\ValueObject\Time($currentTime);

// Output: 2019-10-09 18:31:00
$time->value()->format(
    DoctorI\Shared\Domain\ValueObject\Time::DEFAULT_FORMAT
);
```

#### Uuid

```php
<?php
$uuid = new DoctorI\Shared\Domain\ValueObject\Uuid('822fc079-f521-45ee-8511-c4daf2958d02');

// Output: 822fc079-f521-45ee-8511-c4daf2958d02
$uuid->value();
```

#### Number

```php
<?php
$number = new DoctorI\Shared\Domain\ValueObject\Number(1);

// Output: 1
$number->value();
```

#### Money

```php
<?php
// DEFAULT
$money = new DoctorI\Shared\Domain\ValueObject\Money(1.23);

// Output: 1.23
$money->value();

// Output: 1.23 EUR
$money->_toString();


// DOLLAR
$money = new DoctorI\Shared\Domain\ValueObject\Money(1.23, 'USD');

// Output: 1.23
$money->value();

// Output: 1.23 USD
$money->_toString();
```
