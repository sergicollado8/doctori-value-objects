<?php

declare(strict_types=1);

namespace DoctorI\Tests\Shared\Domain\ValueObject;

use DoctorI\Shared\Domain\ValueObject\Exception\InvalidText;
use DoctorI\Shared\Domain\ValueObject\Text;

final class TextTest extends AbstractTest
{
    protected Text $model;

    protected function setUp(): void
    {
        parent::setUp();

        $this->model = new Text($this->params['text'], true);
    }

    public function testConstruction(): void
    {
        self::assertInstanceOf(Text::class, $this->model);
    }

    public function testConstructWithInvalidValue(): void
    {
        $invalidValue = ' ';
        $this->expectException(InvalidText::class);
        $this->expectExceptionMessage('Text field value < > is not a valid string.');

        new Text($invalidValue, false);
    }

    public function testConstructWithNullableTrue(): void
    {
        $emptyString = '';
        $nullable = true;

        new Text($emptyString, $nullable);

        self::assertTrue($this->model->isNullable());
    }

    public function testGetValue(): void
    {
        self::assertSame($this->params['text'], $this->model->value());
    }

    public function testToString(): void
    {
        self::assertSame($this->params['text'], $this->model->__toString());
    }
}
