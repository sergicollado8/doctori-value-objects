<?php

declare(strict_types=1);

namespace DoctorI\Tests\Shared\Domain\ValueObject;

use DateTime;
use PHPUnit\Framework\TestCase;

abstract class AbstractTest extends TestCase
{
    protected array $params;

    protected function setUp(): void
    {
        parent::setUp();

        $this->params = [
            'email' => 'test@email.com',
            'money' => 123.334,
            'number' => 12345,
            'text' => 'A valid string',
            'time' => new DateTime('2019-10-09 18:31:00'),
            'uuid' => 'a243b026-1065-4802-bc1b-5a256a42727d',
        ];
    }
}
