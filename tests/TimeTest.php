<?php

declare(strict_types=1);

namespace DoctorI\Tests\Shared\Domain\ValueObject;

use DoctorI\Shared\Domain\ValueObject\Time;

class TimeTest extends AbstractTest
{
    protected Time $model;

    protected function setUp(): void
    {
        parent::setUp();

        $this->model = new Time($this->params['time']);
    }

    /**
     * Test construct.
     */
    public function testConstruction(): void
    {
        self::assertInstanceOf(Time::class, $this->model);
    }

    /**
     * Test getRaw() method.
     */
    public function testGetRaw(): void
    {
        self::assertSame(
            $this->params['time']->format(Time::DEFAULT_FORMAT),
            $this->model->value()->format(Time::DEFAULT_FORMAT)
        );
    }

    public function testToString(): void
    {
        self::assertSame($this->params['time']->format(Time::DEFAULT_FORMAT), $this->model->__toString());
    }
}
