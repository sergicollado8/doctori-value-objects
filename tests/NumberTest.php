<?php

declare(strict_types=1);

namespace DoctorI\Tests\Shared\Domain\ValueObject;

use DoctorI\Shared\Domain\ValueObject\Exception\InvalidNumber;
use DoctorI\Shared\Domain\ValueObject\Number;

final class NumberTest extends AbstractTest
{
    protected \DoctorI\Shared\Domain\ValueObject\Number $model;

    protected function setUp(): void
    {
        parent::setUp();

        $this->model = new Number($this->params['number']);
    }

    public function testConstruction(): void
    {
        self::assertInstanceOf(Number::class, $this->model);
    }

    /**
     * @dataProvider rawInvalidNumberProvider
     */
    public function testConstructWithInvalidValue(int $raw, string $exception): void
    {
        $this->expectException(InvalidNumber::class);
        $this->expectExceptionMessage($exception);

        new Number($raw);
    }

    public function testGetValue(): void
    {
        self::assertSame((int)$this->params['number'], $this->model->value());
    }

    public function testToString(): void
    {
        self::assertSame((string)$this->params['number'], $this->model->__toString());
    }

    public function rawInvalidNumberProvider(): array
    {
        return [
            [-1, 'Number field value <-1> is not a valid positive value.']
        ];
    }
}
