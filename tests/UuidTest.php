<?php

declare(strict_types=1);

namespace DoctorI\Tests\Shared\Domain\ValueObject;

use DoctorI\Shared\Domain\ValueObject\Exception\InvalidUuid;
use DoctorI\Shared\Domain\ValueObject\Uuid;
use Ramsey\Uuid\Uuid as RamseyUuid;

final class UuidTest extends AbstractTest
{
    protected Uuid $model;

    protected function setUp(): void
    {
        parent::setUp();

        $this->model = new Uuid($this->params['uuid']);
    }

    public function testConstruction(): void
    {
        self::assertInstanceOf(Uuid::class, $this->model);
    }

    /**
     * @dataProvider rawInvalidUuidProvider
     */
    public function testConstructWithInvalidValue(string $raw, string $exception): void
    {
        $this->expectException(InvalidUuid::class);
        $this->expectExceptionMessage($exception);

        new Uuid($raw);
    }

    public function testGetValue(): void
    {
        self::assertSame($this->params['uuid'], $this->model->value());
    }

    public function testToString(): void
    {
        self::assertSame($this->params['uuid'], $this->model->__toString());
    }

    public function testEquals(): void
    {
        $uuid = Uuid::random();
        self::assertFalse($uuid->equals($this->model));
    }

    public function testRandomUuidIsValid(): void
    {
        $uuid = Uuid::random();
        self::assertTrue(RamseyUuid::isValid($uuid->value()));
    }

    public function rawInvalidUuidProvider(): array
    {
        return [
            [' ', 'Given string < > is not a valid Uuid.'],
            ['123', 'Given string <123> is not a valid Uuid.'],
            ['/132/1322', 'Given string </132/1322> is not a valid Uuid.']
        ];
    }
}
