<?php

declare(strict_types=1);

namespace DoctorI\Tests\Shared\Domain\ValueObject;

use DoctorI\Shared\Domain\ValueObject\Exception\InvalidMoney;
use DoctorI\Shared\Domain\ValueObject\Money;

final class MoneyTest extends AbstractTest
{
    protected Money $model;

    protected function setUp(): void
    {
        parent::setUp();

        $this->model = new Money($this->params['money']);
    }

    public function testConstruction(): void
    {
        self::assertInstanceOf(Money::class, $this->model);
    }

    /**
     * @dataProvider rawInvalidMoneyProvider
     */
    public function testConstructWithInvalidValue(float $raw, string $exception): void
    {
        $this->expectException(InvalidMoney::class);
        $this->expectExceptionMessage($exception);

        new Money($raw);
    }

    public function testGetValue(): void
    {
        self::assertSame(round($this->params['money'], 2), $this->model->value());
    }

    public function testGetValueCurrencyCustom(): void
    {
        $money = new Money(100.99, 'USD');
        self::assertSame('USD', $money->currency());
        self::assertSame(100.99 . ' USD', $money->__toString());
        self::assertSame(100.99, $money->value());
    }

    public function testGetDefaultCurrency(): void
    {
        self::assertSame('EUR', $this->model->currency());
    }

    public function testToString(): void
    {
        self::assertSame(round($this->params['money'], 2) . ' EUR', $this->model->__toString());
    }

    public function rawInvalidMoneyProvider(): array
    {
        return [
            [-1, 'Money field value <-1> is not a valid positive value.']
        ];
    }
}
