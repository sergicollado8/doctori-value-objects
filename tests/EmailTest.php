<?php

declare(strict_types=1);

namespace DoctorI\Tests\Shared\Domain\ValueObject;

use DoctorI\Shared\Domain\ValueObject\Email;
use DoctorI\Shared\Domain\ValueObject\Exception\InvalidEmail;

class EmailTest extends AbstractTest
{
    protected Email $model;

    protected function setUp(): void
    {
        parent::setUp();

        $this->model = new Email($this->params['email']);
    }

    public function testConstruction(): void
    {
        self::assertInstanceOf(Email::class, $this->model);
    }

    /**
     * @dataProvider rawInvalidEmailProvider
     */
    public function testConstructWithInvalidValue(string $raw, string $exception): void
    {
        $this->expectException(InvalidEmail::class);
        $this->expectExceptionMessage($exception);

        new Email($raw);
    }

    public function testGetValue(): void
    {
        self::assertSame($this->params['email'], $this->model->value());
    }

    public function testToString(): void
    {
        self::assertSame($this->params['email'], $this->model->__toString());
    }

    public function rawInvalidEmailProvider(): array
    {
        return [
            [' ', 'The email < > is not a valid email address'],
            ['123', 'The email <123> is not a valid email address'],
            ['@test.com', 'The email <@test.com> is not a valid email address'],
            ['test@.com', 'The email <test@.com> is not a valid email address'],
            ['test@local', 'The email <test@local> is not a valid email address'],
            ['test@local.', 'The email <test@local.> is not a valid email address']
        ];
    }
}
